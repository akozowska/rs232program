﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RS232Connect.Model
{
    static class BaudRate
    {

        public static readonly IList<int> BaudRates = new List<int>()
        {
            300,
            600,
            1200,
            2400,
            9600,
            14400,
            19200,
            38400,
            57600,
            115200,
            128000,
        };
        
    }
}
