﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RS232Connect.Model
{
   static class DataBit
    {
        public readonly static Dictionary<int,List<StopBits>> DataBits = new Dictionary<int, List<StopBits>>
        {
            [5] = new List<StopBits> { StopBits.One, StopBits.OnePointFive},
            [6] = new List<StopBits> { StopBits.One, StopBits.Two },
            [7] = new List<StopBits> { StopBits.One, StopBits.Two },
            [8] = new List<StopBits> { StopBits.One, StopBits.Two },
        };
    }
}
