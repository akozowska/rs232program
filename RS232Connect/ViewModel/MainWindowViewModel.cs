﻿using System.IO.Ports;
using System.Windows.Navigation;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using RS232Connect.Helpers;
using RS232Connect.Messages;
using RS232Connect.Model;

namespace RS232Connect.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.mvvmlight.net
    /// </para>
    /// </summary>
    public class MainWindowViewModel : ViewModelBase
    {
        #region public properties
        public string BaudRate
        {
            get
            {
                return comPorts.BaudRate.ToString();
            }
        }
        public string IsPortOpened
        {
            get
            {
                if (comPorts.MyComPort.IsOpen)
                    return "Opened";
                else
                    return "Closed";
            }
        }
        public string SelectedPort
        {
            get
            {
                return comPorts.PortName;
            }
        }
        #endregion 

        private readonly IWindowFactory windowsFactory;
        private ComPorts comPorts;
        public MainWindowViewModel(IWindowFactory windowFactory)
        {
            this.windowsFactory = windowFactory;
            comPorts = new ComPorts();
            Messenger.Default.Register<ComPortMessage>(this, InitializeComPort);
        }
        private void InitializeComPort(ComPortMessage action)
        {
            if (comPorts.MyComPort != null)
                comPorts.MyComPort.Dispose();
            comPorts = action.serialPort;
            RaisePropertyChanged("BaudRate");
            RaisePropertyChanged("IsPortOpened");
            RaisePropertyChanged("SelectedPort");
        }

        #region Commands
        private RelayCommand openPortSettingsWindow;
        public RelayCommand OpenPortSettingsWindow
        {
            get
            {
                return openPortSettingsWindow ?? (openPortSettingsWindow = new RelayCommand(() =>
                {
                    windowsFactory.CreateNewWindow();
                }));
            }
        }

        private RelayCommand openComPort;
        public RelayCommand OpenComPort
        {
            get
            {
                return openComPort ?? (openComPort = 
                    new RelayCommand(() =>
                    {
                        if (!(comPorts.MyComPort.IsOpen))
                        {
                            comPorts.MyComPort.Open();
                            RaisePropertyChanged("IsPortOpened");
                        }
                      
                    }, () => comPorts.MyComPort != null));
            }
        }
        #endregion

    }
}