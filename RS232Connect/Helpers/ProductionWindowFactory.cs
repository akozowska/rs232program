﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RS232Connect.View;

namespace RS232Connect.Helpers
{
    class ProductionWindowFactory : IWindowFactory
    {
        private PortSettingsView portSettingsView;
        public ProductionWindowFactory()
        {

        }
        public void CreateNewWindow()
        {
            portSettingsView = new PortSettingsView();
            portSettingsView.Show();
        }
     
    }
}
