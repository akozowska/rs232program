﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using RS232Connect.Model;
using GalaSoft.MvvmLight;

using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using RS232Connect.Messages;
using System.Windows;
using System.ComponentModel;
using System.Windows.Data;

namespace RS232Connect.ViewModel
{
   
    class PortSettingsViewModel : ViewModelBase
    {
        private ComPorts comPort;
        private string _selectedPortName;
        public PortSettingsViewModel()
        {
            BaudRates = new List<int>(Model.BaudRate.BaudRates);
            DataBits = new Dictionary<int, List<StopBits>>(Model.DataBit.DataBits);
            SerialPortNames = new ObservableCollection<string>(SerialPort.GetPortNames());
            List<StopBits> StopBit = new List<StopBits>();
        }
        #region Public Properties 
     
        public List<int> BaudRates { get; }
        public Dictionary<int, List<StopBits>> DataBits { get; }
        public ObservableCollection<string> SerialPortNames
        {
            get; set;
        }
        public IEnumerable<Handshake> Handshake
        {
            get
            {
                return Enum.GetValues(typeof(Handshake)).Cast<Handshake>();
            }
        }
       
        public List<StopBits> StopBit
        {
            get
            {
                return DataBits[SelectedDataBits];
                
            }
            
        }
        public IEnumerable<Parity> ParityBits
        {
            get
            {
                return Enum.GetValues(typeof(Parity)).Cast<Parity>();
            }
        }
        public string SelectedPortName
        {
            get
            {
                return _selectedPortName;
                
            }
                 set
            {
                _selectedPortName = value;
                RaisePropertyChanged("SelectedPortName");
            }
        }
        public int SelectedBaudRate
        {
            get; set;
        }
        public Parity SelectedParityBits
        {
            get; set;
        }
       
        public Handshake SelectedHandshake
        {
            get; set;
        }

        public StopBits SelectedStopBits
        {
            get; set;
        }
        private int _selectedDataBits;
        public int SelectedDataBits
        {
            get
            {
                return _selectedDataBits;
            }

             set
            {
                _selectedDataBits = value;
                RaisePropertyChanged("StopBit");
            }
        }
        #endregion
        #region Commands
        private RelayCommand cancelPortSettingsChanges;
        public ICommand CancelPortSettingsChangesCommand
        {
            get { return cancelPortSettingsChanges ?? (cancelPortSettingsChanges = new RelayCommand(CancelPortSettingsChanges)); }
        }

        private void CancelPortSettingsChanges()
        {
            NotifyWindowsToClose();  
        }

        private RelayCommand commitPortSettingsChanges;
        public RelayCommand CommitPortSettingsChangesCommand
        {
            get
            {
                return commitPortSettingsChanges ?? (commitPortSettingsChanges = new RelayCommand(CommitChanges, () => !String.IsNullOrEmpty(SelectedPortName)));
            }
            
        }
        private void CommitChanges()
        {
            comPort = new ComPorts()
            {
                PortName = SelectedPortName,
                BaudRate = SelectedBaudRate,
                ParityBits = SelectedParityBits,
                DataBits = SelectedDataBits,
                StopBits = SelectedStopBits,
                Handshake = SelectedHandshake,
            };
            comPort.MyComPort = comPort.CreateSerialPort();
            Messenger.Default.Send<ComPortMessage>(new ComPortMessage(comPort));
            NotifyWindowsToClose();

        }
        #endregion
        private void NotifyWindowsToClose()
        {
            Messenger.Default.Send<NotificationMessage>(new NotificationMessage(this, "CloseWindowsBoundToMe"));
        }
    }


}
