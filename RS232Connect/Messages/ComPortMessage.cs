﻿using RS232Connect.Model;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RS232Connect.Messages
{
    class ComPortMessage
    {
        public ComPorts serialPort;
        public ComPortMessage(ComPorts serialPort)
        {
            this.serialPort = serialPort;
        }
    }
}
