﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RS232Connect.Helpers
{
    public interface IWindowFactory
    {
        void CreateNewWindow();
    }
}
