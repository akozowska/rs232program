﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Runtime.Remoting.Services;

namespace RS232Connect.Model
{
    public class ComPorts
    {
        public SerialPort MyComPort;
        public string PortName { get; set; } 
        public int BaudRate { get; set; } 
        public Parity ParityBits { get; set; } 
        public int DataBits { get; set; } 
        public StopBits StopBits { get; set; }
        public Handshake Handshake { get; set; }
        public ComPorts()
        {
        }
        public SerialPort CreateSerialPort()
        {
                return new SerialPort()
                {
                    PortName = this.PortName,
                    BaudRate = this.BaudRate,
                    Parity = this.ParityBits,
                    DataBits = this.DataBits,
                    StopBits = this.StopBits,
                    Handshake = this.Handshake,
                };
        
        }
       
     
        

    }
}
